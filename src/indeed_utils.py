import requests

from src.config import CLIENT_ID, REQUEST



def get_candidate_list(query, location, start = 0):
    """
    Example https://auth.indeed.com/resumes/c81a41a6b3b13b69?client_id=f6750f051efa17d4d70b3b704a71f218dc06b17d2bdb771b54440375addb6209&v=1
    :param query: For example: java developer
    :param location: For example: london
    :param start: Start results at this result number, beginning with 0. Default is 0
    :return: nested dictionary.
    """
    query = query.lower().strip().replace(" ", "+")
    parameters = {'client_id': CLIENT_ID, "v": 1, "q": query, "l": location, "start": start}
    response = requests.get(REQUEST, params=parameters)
    data = response.json()
    return data

def get_candidate_infor(candidate_dict, fields):
    """
    :param candidate_dict: A dictionary containing candidate's information
    :param field_dict: A dictionary whose keys are field in Indeed, values are dataframe column's name
    :return: A list which have the same length and order as field_dict.values()
    """
    return [candidate_dict[field] for field in fields]

# def get_candidate_resume(candidate_id):
#     """
#     Example https://auth.indeed.com/resumes?client_id=f6750f051efa17d4d70b3b704a71f218dc06b17d2bdb771b54440375addb6209&v=1&q=java+developer&l=london
#     :param candidate_id:
#     :return:
#     """
#     parameters = {'client_id': CLIENT_ID, "v": 1}
#     response = requests.get("https://auth.indeed.com/resumes/{}".format(candidate_id), params=parameters)
#     data = response.json()
#     return data


