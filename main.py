from src.indeed_utils import get_candidate_list, get_candidate_infor
from src.config import INDEED2DF
import pandas as pd
from collections import OrderedDict

def get_indeed_profiles(query, location):

    indeed2df = OrderedDict(INDEED2DF)
    indeed_fields = list(indeed2df.keys())
    df_fields = list(indeed2df.values())
    df_list = []
    start_index = 0
    while True:
        candidates_list = get_candidate_list(query, location, start=start_index)["data"]["resumes"]
        num_resumes = len(candidates_list)
        if num_resumes == 0:
            break
        print ("Get {} resumes".format(num_resumes))
        candidates_list = [get_candidate_infor(list_, indeed_fields) for list_ in candidates_list]
        df_list.append(pd.DataFrame(candidates_list, columns=df_fields))
        start_index += 50
    df = pd.concat(df_list, ignore_index=True)
    # Uncomment the following line if you want to output a pickle file
    # df.to_pickle("indeed_resumes.pkl")
    return df

if __name__ == '__main__':
    query = "data analyst"
    location = "london"
    get_indeed_profiles(query, location)